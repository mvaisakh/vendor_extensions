#
# Copyright (C) 2022 Vaisakh Murali
# SPDX-License-Identifer: Apache-2.0
#

# Build support for extras
PRODUCT_PACKAGES += \
	Chromium \
	ThemePicker \
	FrameworkExtensions \
	SystemUIExtensions

# Font
include vendor/extensions/fonts/fonts.mk
